package harkkatyö;

import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.events.EventListener;
import org.w3c.dom.events.EventTarget;

import javafx.fxml.Initializable;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.control.ListView;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;

public class UIController implements Initializable{
	@FXML AnchorPane anchorPane;
	@FXML WebView webView;
	@FXML TabPane tabPane;
	
	//tab pääohjelma
	@FXML Label mainLabel;
	@FXML ComboBox comboBoxStart;
	@FXML ComboBox comboBoxEnd;
	@FXML Button buttonSwitcheroo;
	@FXML TextField textFieldStart;
	@FXML TextField textFieldEnd;
	@FXML ComboBox comboBoxItem;
	@FXML ComboBox comboBoxClass;
	@FXML ComboBox comboBoxPackage;
	
	@FXML TextField textFieldItemName;
	@FXML TextField textFieldItemLength;
	@FXML TextField textFieldItemWidth;
	@FXML TextField textFieldItemHeight;
	@FXML TextField textFieldItemVolume;
	@FXML TextField textFieldItemWeight;
	
	@FXML TextField textFieldPackageSize;
	@FXML TextField textFieldPackageWeight;
	
	@FXML Label labelShowDist;
	@FXML Label labelItemName;
	@FXML Label labelItemDimensions;
	@FXML Label labelItemWeight;
	@FXML Label labelItemDescription;
	@FXML Label labelItemError;
	@FXML Label labelSendInfo;
	@FXML Label labelPackage;
	@FXML Label labelPackageSize;
	@FXML Label labelPackageWeight;
	
	@FXML RadioButton radioButtonBreakable;
	@FXML TextArea textAreaItemDescription;
	@FXML ListView<String> listViewClassInfo;
	@FXML ListView listViewPackageContent;
	
	@FXML Button buttonRemoveItem;
	@FXML Button buttonUpdateItem;
	@FXML Button buttonSaveItem;
	@FXML Button buttonSend;
	@FXML Button buttonAddToPackage;
	@FXML Button buttonRemoveItemFromPackage;
	@FXML Button buttonRemovePaths;
	@FXML Button buttonPackageDelete;
	@FXML Button buttonPackageUpdate;
	@FXML Button buttonPackageAdd;
	
	//tab historia
	@FXML ListView listViewHistory;
	@FXML ListView listViewHistoryStart;
	@FXML ListView listViewHistoryEnd;
	@FXML ListView listViewHistoryItem;
	@FXML Button buttonRemoveFromHistory;
	@FXML Button buttonEditHistory;
	@FXML DatePicker datePickerHistory;
	
	//tab asetukset
	@FXML Button buttonDefaultDatabase;
	
	//muut asiat
	private SQLManager sqlMan = SQLManager.getInstance();
	private ListManager listMan = ListManager.getInstance();
	private final Items newItem = new Items("Uusi esine", "Uutta esinettä ei voi poistaa, eikä päivittää. Se on olemassa pohjana uudelle esineelle");
	private Package pack = new Package();
	private LogWriter log = new LogWriter();
	
	//tab pääohjelma	
	
	@FXML public void onTextFieldStart(ActionEvent event) {
		//feel good metodi, joka saa käyttäjän tuntemaan, että enterin painaminen teki jotain. Auttaa myös textFieldin jättämisessä tyhjäksi
		anchorPane.requestFocus();
	}
	
	@FXML public void onTextFieldEnd(ActionEvent event) {
		//feel good metodi, joka saa käyttäjän tuntemaan, että enterin painaminen teki jotain. Auttaa myös textFieldin jättämisessä tyhjäksi
		anchorPane.requestFocus();
	}
	
	public void clearMarkers() {
		try {
			//hidastettu ohjelman toimintaa, jotta vältytään oudoilta ongelmilta, jossa markerit eivät katoa kartalta
			Thread.sleep(200);
		} catch (InterruptedException ie) {
			System.out.println("Jokin keskeytti odottamisen");
		}
		webView.getEngine().executeScript("document.clearMarkers()");
		
	}
	
	public void addMarkers(ActionEvent event) {
		//suorittaa metodin addPoint(), joka piirtää merkit kartalle. Lisäksi jos kaksi merkkiä on piirretty, niin palauttaa niiden välisen etäisyyden
		//aluksi poistetaan vanhat merkit ja reitit
		clearMarkers();
		onButtonRemovePaths(event);
		Pakettimaatti ps = (Pakettimaatti)comboBoxStart.getValue();
		Pakettimaatti pe = (Pakettimaatti)comboBoxEnd.getValue();
		labelShowDist.setText("");
		//merkkien piirtäminen
		if(ps != null) {
			addPoint(ps, "blue");
		}
		if(pe != null) {
			addPoint(pe, "blue");
		}
		//pituuden laskeminen
		if(ps != null && pe != null) {
			String format = "%.7f";
			String dist = String.valueOf(webView.getEngine().executeScript("document.calculateLength('" + 
			String.format(format, ps.getLat()) + "', '" + 
			String.format(format, ps.getLng()) + "', '" +
			String.format(format, pe.getLat()) + "', '" +
			String.format(format, pe.getLng()) + "')"));
			labelShowDist.setText("Etäisyys: " + dist + "km");
		}
	}
	
	public void addPoint(Pakettimaatti p, String color) {
		//muuttaa Pakettimaatin tiedot oikeaan muotoon javascriptiä varten ja suorittaa javascriptin
		String osoite = p.getOsoite() + ", " + p.getPostinumero() + ", " + p.getKaupunki();
		String tiedot = "<p> " + p.getRakennus() + ", " + p.getTyyppi() + "<br /> Avoinna: ";
		for(Entry<String, String> e : p.getAika().entrySet()) {
			tiedot += "<br />" + e.getKey() + ": " + e.getValue();
		}
		tiedot += "</p>";
		System.out.println(osoite);
		webView.getEngine().executeScript("document.goToLocation('" + osoite + "', '" + tiedot + "', '" + color + "')");
		System.out.println("lisätty piste kartalle");
	}
	
	@FXML public void onComboBoxStartAction(ActionEvent event) {
		//merkit lisätään/poistetaan automaattisesti kartalta riippuen mitä comboBoxista valitaan. Lisäksi kutsutaan onComboBoxClass,
		//joka päivittää labelin, joka kertoo onko lähetyksessä jotain vikaa
		System.out.println("onComboBoxStartAction");
		addMarkers(event);
		onComboBoxClass(event);
	}
	
	@FXML public void onComboBoxEndAction(ActionEvent event) {
		//sama kuin onComboBoxStartAction
		System.out.println("onComboBoxEndAction");
		addMarkers(event);
		onComboBoxClass(event);
	}
	
	@FXML public void onButtonSwitcheroo(ActionEvent event) {
		//vaihtaa lähtöpaikan ja päämäärän toistepäin. Tätä varten täytyy tilapäisesti laittaa comboBoxien action nulliksi, 
		//tai muuten merkit piirrettäisiin ja poistettaisiin moneen kertaan turhaan
		EventHandler<ActionEvent> handlerStart = comboBoxStart.getOnAction();
		EventHandler<ActionEvent> handlerEnd = comboBoxEnd.getOnAction();
		comboBoxStart.setOnAction(null);
		comboBoxEnd.setOnAction(null);
		
		System.out.println("switcheroo");
		Pakettimaatti p = (Pakettimaatti)comboBoxStart.getValue();
		comboBoxStart.setValue(comboBoxEnd.getValue());
		comboBoxEnd.setValue(p);
		
		comboBoxStart.setOnAction(handlerStart);
		comboBoxEnd.setOnAction(handlerEnd);
		addMarkers(event);
	}
	
	@FXML public void onWebViewContextMenuRequested(ContextMenuEvent event) {
		//turha, mutta jos haluaa avata jonkinlaisen menun kartalla, niin tätä voisi käyttää siihen
		System.out.println("Oikea klikkaus");
	}
	
	
	@FXML public void onComboBoxStartShowing(Event event) {
		//comboBoxin sisältö määritellään hetkeä ennen se avaamista. Tässä otetaan huomioon textField, joka rajaa
		//comboBoxin sisältöä. Sisältö rajoitetaan osoitteen ja kaupungin mukaan
		String s = textFieldStart.getText().toLowerCase();
		if(s.equals("") == false) {
			comboBoxStart.getItems().clear();
			for(Pakettimaatti p : listMan.getAl()) {
				if (p.getKaupunki().toLowerCase().contains(s) || p.getOsoite().toLowerCase().contains(s)) {
					comboBoxStart.getItems().add(p);
				}
			}
		} else {
			comboBoxStart.getItems().clear();
			comboBoxStart.getItems().addAll(listMan.getAl());
		}
		//jos toisessa comboBoxissa on jo valinta, niin se poistetaan tästä comboBoxista, jotta paketteja ei voisi lähettää automaatista itseensä
		if(!comboBoxEnd.getSelectionModel().isEmpty()) {
			comboBoxStart.getItems().remove(comboBoxEnd.getValue());
		}
	}
	
	@FXML public void onComboBoxEndShowing(Event event) {
		//sama kuin onComboBoxStartShowing, mutta eri comboBoxille
		String s = textFieldEnd.getText().toLowerCase();
		if(s.equals("") == false) {
			comboBoxEnd.getItems().clear();
			for(Pakettimaatti p : listMan.getAl()) {
				if (p.getKaupunki().toLowerCase().contains(s) || p.getOsoite().toLowerCase().contains(s)) {
					comboBoxEnd.getItems().add(p);
				}
			}
		} else {
			comboBoxEnd.getItems().clear();
			comboBoxEnd.getItems().addAll(listMan.getAl());
		}
		if(!comboBoxStart.getSelectionModel().isEmpty()) {
			comboBoxEnd.getItems().remove(comboBoxStart.getValue());
		}
	}
	

	
	@FXML public void onComboBoxItem(ActionEvent event) {
		//esine valitaan comboBoxista ja sen tiedot täydennetään alla oleviin kenttiin käyttöliittymässä
		String format = "%.4f";
		Items i = (Items)comboBoxItem.getValue();
		boolean b = false;
		
		setLabelItemError("", false, "black");
		
		if(i != null) {
			textFieldItemName.setText(i.getNimi());
			textFieldItemLength.setText(String.format(format, i.getPituus()));
			textFieldItemWidth.setText(String.format(format,i.getLeveys()));
			textFieldItemHeight.setText(String.format(format,i.getKorkeus()));
			textFieldItemVolume.setText(String.format("= %.5f", i.getPituus() * i.getLeveys() * i.getKorkeus()) + "m\u00B3");
			textFieldItemWeight.setText(String.format(format,i.getPaino()));
			radioButtonBreakable.selectedProperty().set(b = (i.getHajoava() == 1));
			textAreaItemDescription.setText(i.getMuuta());
			//määritetään esineeseen liittyvät nappulat
			buttonUpdateItem.setDisable(true);
			buttonSaveItem.setDisable(true);
			buttonRemoveItem.setDisable(false);
		}
	}
	
	@FXML public void onTextFieldItemKeyReleased(KeyEvent event) {
		//painoon ja esineiden mittoihin liittyvä metodi, joka visuaalisesti värien kautta kertoo käyttäjälle, jos hänen syöttämänsä luku on virheellinen
		double length = 0, width = 0, height = 0, weight = 0;
		setLabelItemError("", false, "black");
		try {
			length = Double.parseDouble(textFieldItemLength.getText());
			textFieldItemLength.setStyle("-fx-text-fill: black;");
		} catch (NumberFormatException|NullPointerException nfe) {
			textFieldItemLength.setStyle("-fx-text-fill: red;");
		}
		try {
			width = Double.parseDouble(textFieldItemWidth.getText());
			textFieldItemWidth.setStyle("-fx-text-fill: black;");
		} catch (NumberFormatException|NullPointerException nfe) {
			textFieldItemWidth.setStyle("-fx-text-fill: red;");
		}
		try {
			height = Double.parseDouble(textFieldItemHeight.getText());
			textFieldItemHeight.setStyle("-fx-text-fill: black;");
		} catch (NumberFormatException|NullPointerException nfe) {
			textFieldItemHeight.setStyle("-fx-text-fill: red;");
		}
		try {
			weight = Double.parseDouble(textFieldItemWeight.getText());
			textFieldItemWeight.setStyle("-fx-text-fill: black;");
		} catch (NumberFormatException|NullPointerException nfe) {
			textFieldItemWeight.setStyle("-fx-text-fill: red;");
		}
		if(length * width * height != 0) {
			textFieldItemVolume.setText("= " + String.format("%.5f", length * width * height) + "m\u00B3");
		} else {
			textFieldItemVolume.setText("Laskuvirhe");
		}
	}
	

	
	@FXML public void onComboBoxClass(ActionEvent event) {
		//tarkistetaan onko luokkaa valittu
		if(comboBoxClass.getSelectionModel().isEmpty()) {
			return;
		}
		
		boolean b;
		labelSendInfo.setText("");
		SendClass sc = (SendClass) comboBoxClass.getValue();
		String sizeLimit = "";
		String weightLimit = "";
		String distLimit = "";
		String speed = "";
		String breakable = "";
		String description = "";
		
		//Lisätään kuvaukset lähetysluokkiin
		if(sc.getLuokka() == 1) {
			description = "Tyyppi: Pikatoimitus";
		} else if(sc.getLuokka() == 2) {
			description = "Tyyppi: Turvakuljetus";
		} else if(sc.getLuokka() == 3) {
			description = "Tyyppi: Stressinpurkukuljetus";
		}
		
		//virheen tarkistus ja luokan eri rajoitusten tarkistaminen. Virheet ilmoitetaan käyttäjälle labelin kautta
		if(sc.getKokoraja() == -1) {
			sizeLimit = "Kokorajoitus: Ei rajoitusta";
		} else {
			sizeLimit = "Kokorajoitus: " + String.format("%.3f", sc.getKokoraja()) + "m\u00B3";
			try {
				if(sc.getKokoraja() < pack.getCompleteSize()) {
					System.out.println(sc.getKokoraja() + " " + pack.getCompleteSize());
					labelSendInfo.setText(labelSendInfo.getText() + "Paketti on liian suuri\n");
				}
			} catch(NumberFormatException nfe) {
				labelSendInfo.setText(labelSendInfo.getText() + "Paketilla ei ole kokoa\n");
			}
		}
		if(sc.getPainoraja() == -1) {
			weightLimit = "Painorajoitus: Ei rajoitusta";
		} else {
			weightLimit = "Painorajoitus: " + String.format("%.1f", sc.getPainoraja()) + "kg";
			try {
				if(sc.getPainoraja() < pack.getCompleteWeight()) {
					labelSendInfo.setText(labelSendInfo.getText() + "Paketti on liian painava\n");
				}
			} catch(NumberFormatException nfe) {
				labelSendInfo.setText(labelSendInfo.getText() + "Paketilla ei ole painoa\n");
			}
		}
		if(sc.getEtäisyysraja() == -1) {
			distLimit = "Etäisyysrajoitus: Ei rajoitusta";
		} else {
			distLimit = "Etäisyysrajoitus: " + String.format("%.1f", sc.getEtäisyysraja() / 1000) + "km";
			try {
				if((sc.getEtäisyysraja() / 1000) < Double.parseDouble(labelShowDist.getText().replaceAll("[^0-9, \\.]|", ""))) {
					labelSendInfo.setText(labelSendInfo.getText() + "Matka on liian pitkä\n");
				}
			} catch(NumberFormatException nfe) {
				labelSendInfo.setText(labelSendInfo.getText() + "Ongelmia matkan laskemisessa\n");
			}
		}
		speed = "Nopeus: " + String.format("%.1f", sc.getNopeus());
		
		//kerrotaan käyttäjälle, jos esineitä tulee hajoamaan
		breakable = "Hajottava: " + ((b = (sc.getHajottava() == 1))? "Kyllä":"Ei");
		boolean packHasBreakableItems = false;
		for(Items item : pack.getItems()) {
			if(item.getHajoava() == 1) {
				packHasBreakableItems = true;
			}
		}
		if(sc.getHajottava() == 1 && packHasBreakableItems) {
			labelSendInfo.setText(labelSendInfo.getText() + "Esineitä tulee hajoamaan matkan aikana\n");
		}
		
		//päivitetään listView näyttämään luokan tiedot
		listViewClassInfo.getItems().clear();
		listViewClassInfo.getItems().add(sizeLimit);
		listViewClassInfo.getItems().add(weightLimit);
		listViewClassInfo.getItems().add(distLimit);
		listViewClassInfo.getItems().add(speed);
		listViewClassInfo.getItems().add(breakable);
		listViewClassInfo.getItems().add(description);
	}
	

	
	
	@FXML public void onButtonRemoveItem(ActionEvent event) {
		//esineen poistaminen tietokannasta, listasta ja comboBoxista
		//virheentarkistus
		if(comboBoxItem.getSelectionModel().isEmpty()) {
			return;
		}
		Items item = (Items)comboBoxItem.getValue();
		if(item.equals(newItem)) {
			setLabelItemError("Uutta esinettä ei voi poistaa", true, "red");
			System.out.println("Uutta esinettä ei voi poistaa");
			return;
		}
		//itse poistaminen
		sqlMan.deleteItemFromDatabase(item);
		listMan.getIl().remove(item);
		comboBoxItem.setValue(null);
		comboBoxItem.getItems().remove(item);
		
		//nollataan textFieldien ja muiden arvot
		textFieldItemName.setText(null);
		textFieldItemLength.setText(null);
		textFieldItemWidth.setText(null);
		textFieldItemHeight.setText(null);
		textFieldItemVolume.setText(null);
		textFieldItemWeight.setText(null);
		textAreaItemDescription.setText(null);
		radioButtonBreakable.selectedProperty().set(false);
		
		//jos esine on paketissa, niin se poistetaan sieltäkin
		pack.removeAll(item);
		for(Package p : listMan.getPl()) {
			p.removeAll(item);
		}
		
		//päivitetään listViewPackage ja säädetään esine nappulat
		updatePackageListView(event);
		buttonRemoveItem.setDisable(true);
		buttonUpdateItem.setDisable(true);
		buttonSaveItem.setDisable(true);
		
		setLabelItemError("Esine poistettu", true, "white");
		System.out.println("Esine poistettu. Id = " + item.getId());
		
	}
	
	@FXML public void onButtonUpdateItem(ActionEvent event) {
		//päivittää esineen tiedot arrayListaan, comboBoxiin, pakettiin ja tietokantaan. Muuttaa siis olemassa olevien lähetysten tietoja, sinänsä vaarallista
		//uuden esineen poikkeus
		String name = textFieldItemName.getText();
		if(comboBoxItem.getValue().equals(newItem)) {
			setLabelItemError("Uuteen esineeseen ei voi tehdä muutoksia", true, "red");
			System.out.println("Uuteen esineeseen ei voi tehdä muutoksia");
			return;
		}
		//rakennetaan esine sen tiedoista, ilmoitetaan käyttäjälle virheestä, jos esineen tiedoissa on sellainen
		try {
			double length = Double.parseDouble(textFieldItemLength.getText());
			double width= Double.parseDouble(textFieldItemWidth.getText());
			double height = Double.parseDouble(textFieldItemHeight.getText());
			double volume = Double.parseDouble(textFieldItemVolume.getText().substring(1, textFieldItemVolume.getText().length() - 2));
			if(volume == 0) {
				setLabelItemError("Esineellä ei ole kokoa", true, "red");
				NumberFormatException ex = new NumberFormatException();
				throw ex;
			}
			double weight = Double.parseDouble(textFieldItemWeight.getText());
			if(weight == 0) {
				setLabelItemError("Esineellä ei ole painoa", true, "red");
				NumberFormatException ex = new NumberFormatException();
				throw ex;
			}
			//muutetaan radiobuttonin arvo int:iksi
			int breakable = radioButtonBreakable.isSelected()? 1 : 0;
			String desc = textAreaItemDescription.getText();
			Items item = (Items)comboBoxItem.getValue();
			
			
			Items s = item;
			System.out.println(s.getId() + s.getNimi() + s.getPituus() + s.getLeveys() + s.getKorkeus() + s.getPaino() + s.getHajoava() + s.getMuuta());
			
			//etsitään esine listasta ja päivitetään sen tiedot
			int index = listMan.getIl().indexOf(item);
			listMan.getIl().set(index, item);
			
			item.setNimi(name);
			item.setPituus(length);
			item.setLeveys(width);
			item.setKorkeus(height);
			item.setPaino(weight);
			item.setHajoava(breakable);
			item.setMuuta(desc);
			
			//päivitetään comboBoxin valinta
			comboBoxItem.getItems().set(comboBoxItem.getSelectionModel().getSelectedIndex(), item);
			//päivitetään paketin tiedot
			pack.setItem(item);
			int packIndex = comboBoxPackage.getSelectionModel().getSelectedIndex();
			comboBoxPackage.getSelectionModel().select(packIndex);
			updatePackageListView(event);
			//tietokannan päivittäminen
			sqlMan.updateItemInDatabase(item);
			//update nappulan poistaminen käytöstä, kunnes tehdään uusia muutoksia
			buttonUpdateItem.setDisable(true);
			setLabelItemError("Esineen tiedot on päivitetty", true, "white");
			System.out.println("Esineen tiedot päivitetty");
		} catch(NumberFormatException nfe) {
			setLabelItemError("Esineen tiedoissa on virhe", true, "red");
		}
		
	}
	@FXML public void onButtonSaveItem(ActionEvent event) {
		//esineen tallentaminen uutena esineenä arrayListaan, comboBoxiin ja tietokantaan
		//rakennetaan esine sen tiedoista, käytännössä sama juttu kuin esineen päivittäminen, mutta esineen tietoja ei tarvitse muuttaa niin moneen paikkaan
		try {
			String name = textFieldItemName.getText();
			double length = Double.parseDouble(textFieldItemLength.getText());
			double width= Double.parseDouble(textFieldItemWidth.getText());
			double height = Double.parseDouble(textFieldItemHeight.getText());
			double volume = Double.parseDouble(textFieldItemVolume.getText().substring(1, textFieldItemVolume.getText().length() - 2));
			if(volume == 0) {
				setLabelItemError("Esineellä ei ole kokoa", true, "red");
				NumberFormatException ex = new NumberFormatException();
				throw ex;
			}
			double weight = Double.parseDouble(textFieldItemWeight.getText());
			if(weight == 0) {
				setLabelItemError("Esineellä ei ole Painoa", true, "red");
				NumberFormatException ex = new NumberFormatException();
				throw ex;
			}
			int breakable = radioButtonBreakable.isSelected()? 1 : 0;
			String desc = textAreaItemDescription.getText();
			Items item = new Items(name, length, width, height, weight, breakable, desc);
			sqlMan.addItemToDatabase(item);
			listMan.getIl().add(item);
			comboBoxItem.getItems().add(item);
			comboBoxItem.setValue(item);
			setLabelItemError("Uusi esine lisätty", true, "white");
			System.out.println("Uusi esine lisätty");
		} catch(NumberFormatException nfe) {
			setLabelItemError("Esineen tiedoissa on virhe", true, "red");
		}
	}
	
	public void setLabelItemError(String s, boolean b, String color) {
		//metodi joka määrittää itemErrorLabelin 
		labelItemError.setText(s);
		labelItemError.setStyle("-fx-text-fill: " + color + ";");
		labelItemError.setVisible(b);
	}
	
	
	public void updatePackageTextFields() {
		//päivittää paketin koon ja painon
		textFieldPackageSize.setText(String.format("%.5f", pack.getCompleteSize()) + "m\u00B3");
		textFieldPackageWeight.setText(String.format("%.4f", pack.getCompleteWeight()) + "kg");
	}
	
	@FXML public void onButtonRemoveItemFromPackage(ActionEvent event) {
		//poistaa esineen paketin esine kokoonpanosta
		Object o = listViewPackageContent.getSelectionModel().getSelectedItem();
		listViewPackageContent.getItems().remove(o);
		pack.removeItem(o);
		updatePackageTextFields();
		//tarkistetaan uudestaan luokan rajoitukset
		onComboBoxClass(event);
	}
	
	@FXML public void onButtonAddToPackage(ActionEvent event) {
		//lisää esineen paketin esine kokoonpanoon
		//virheentarkistus
		if( comboBoxItem.getSelectionModel().isEmpty()) {
			return;
		}
		if(comboBoxItem.getValue().equals(newItem)) {
			return;
		}
		//esineen muokatut tiedot tallennetaan ennen lisäämistä
		if(!buttonUpdateItem.isDisable()) {
			onButtonUpdateItem(event);
		}
		pack.addItem((Items)comboBoxItem.getValue());
		listViewPackageContent.getItems().clear();
		listViewPackageContent.getItems().addAll(pack.getItems());
		updatePackageTextFields();
		//tarkistetaan uudestaan luokan vaatimukset
		onComboBoxClass(event);
	}
	
	public void updatePackageListView(ActionEvent event) {
		//päivittää listViewin tiedot vastaamaan paketin tietoja
		listViewPackageContent.getItems().clear();
		listViewPackageContent.getItems().addAll(pack.getItems());
		updatePackageTextFields();
		//tarkistetaan uudestaan luokan vaatimukset
		onComboBoxClass(event);
	}
	
	@FXML public void onComboBoxPackageShowing(Event event) {
		//päivittää comboBoxin sisällön juuri ennen kuin se näytetään käyttäjälle. Aiheuttaa mahdollisesti oudot harmaat laatikot ja
		//sen että comboBoxin popupin koko muuttuu oudosti
		comboBoxPackage.getItems().clear();
		comboBoxPackage.getItems().addAll(listMan.getPl());
		ActionEvent acEvent = new ActionEvent();
		updatePackageListView(acEvent);
	}
	
	@FXML public void onButtonSend(ActionEvent event) {
		//paketin lähettäminen, lisätään paketti, reitti, ja toimitus tietokantaan, javascriptin suorittaminen
		//virheentarkistus
		if(comboBoxStart.getSelectionModel().isEmpty() || 
			comboBoxEnd.getSelectionModel().isEmpty() || 
			comboBoxClass.getSelectionModel().isEmpty()) {
			System.out.println("Ei voida lähettää:" + 
					"\nalku: " + comboBoxStart.getSelectionModel().isEmpty() + 
					"\nloppu: " + comboBoxEnd.getSelectionModel().isEmpty() + 
					"\nluokka: " + comboBoxClass.getSelectionModel().isEmpty());
			return;
		}
		
		//määritetään asiat, jotka lähetetään javascriptille
		String color = "blue";
		Pakettimaatti ps = (Pakettimaatti) comboBoxStart.getValue();
		Pakettimaatti pe = (Pakettimaatti) comboBoxEnd.getValue();
		ArrayList<Double> coordinates = new ArrayList<Double>();
		coordinates.add(ps.getLat());
		coordinates.add(ps.getLng());
		coordinates.add(pe.getLat());
		coordinates.add(pe.getLng());
		
		//paketin sisältämien esineiden nimet lisätään stringiin
		String content = "";
		for(Items item : pack.getItems()) {
			content += item.getNimi() + " ";
		}
		SendClass sc = (SendClass) comboBoxClass.getValue();
		
		//tarkistetaan vielä, että luokan sisältämät rajoitukset toteutuvat. Tietyissä tilanteissa onComboBoxClass() sisältämät tarkistukset eivät riitä
		if(sc.getKokoraja() < pack.getCompleteSize() && sc.getKokoraja() != -1) {
			System.out.println("Kokorajavaatimukset eivät täyty");
			return;
		}
		
		if(sc.getPainoraja() < pack.getCompleteWeight() && sc.getPainoraja() != -1) {
			System.out.println("Painorajavaatimukset eivät täyty");
			return;
		}
		
		try {
			if((sc.getEtäisyysraja() / 1000) < Double.parseDouble(labelShowDist.getText().replaceAll("[^0-9, \\.]|", "")) && sc.getEtäisyysraja() != -1) {
				System.out.println("Matka on liian pitkä\n");
				return;
			}
		} catch(NumberFormatException nfe) {
			System.out.println("Ongelmia matkan laskemisessa\n");
		}
		//poistetaan mahdollinen aikaisempi reitti ja juoksija
		onButtonRemovePaths(event);
		webView.getEngine().executeScript("document.clearShreks()");
		
		//kirjoitetaan databaseen osat, joista toimitus koostuu
		int packId = sqlMan.addPackageToDatabase(sc);
		sqlMan.addPileToDatabase(pack, packId);
		int routeId = sqlMan.addRouteToDatabase(ps, pe);
		
		//paketin lisääminen listaan, joka sisältää kaikki aikaisemmat paketit
		Package newPackage = new Package(packId, pack.getItems());
		listMan.getPl().add(newPackage);
		System.out.println(listMan.getPl());

		//luodaan timestamp toimitukselle. Tässä voisi myös käyttää tietokannan omaa aikaleimatoiminnallisuutta
		Date date = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String time = ft.format(date);
		//kirjoitetaan itse toimitus databaseen
		sqlMan.addDeliveryToDatabase(routeId, packId, time);
		
		//tallennetaan sama toimitus myös arrayListaan ja lisätään se historiaan
		ArrayList<Items> list = new ArrayList<Items>();
		list.addAll(pack.getItems());
		Delivery d = new Delivery(ps, pe, sc, list, time, packId, routeId);
		listMan.getDl().add(d);
		listViewHistory.getItems().add(d);
		
		webView.getEngine().executeScript("document.createPath(" + coordinates + ", '" + color + "', '" + sc.getNopeus() + "')");
		System.out.println(coordinates.toString());
		System.out.println("Paketti lähetetty: " + ps.getKaupunki() + " -> " + pe.getKaupunki() + "\nPaketin sisältö: " + content + "\nLuokka: " + sc.getLuokka());
		log.writeToLog("Paketti lähetetty: " + ps.getKaupunki() + "->" + pe.getKaupunki() + " Luokka: " + sc.getLuokka()  + " Paketin sisältö: " + content);
	}
	
	@FXML public void onButtonRemovePaths(ActionEvent event) {
		//poistaa kaikki merkinnät ja reitit kartalta
		webView.getEngine().executeScript("document.deletePaths()");
		webView.getEngine().executeScript("document.clearShreks()");
		System.out.println("Reitit poistettu");
	}
	
	@FXML public void onComboBoxPackage(ActionEvent event) {
		//päivittää listViewPackageContent vastaamaan valittua pakettia
		if(comboBoxPackage.getSelectionModel().isEmpty()) {
			return;
		}
		listViewPackageContent.getItems().clear();
		pack = (Package)comboBoxPackage.getSelectionModel().getSelectedItem();
		updatePackageListView(event);
	}
	
	@FXML public void onListViewPackageContent(MouseEvent event) {
		//valitsee esineen klikattaessa ja valitsee sen esine comboBoxista. Tämä saa aikaan comboBoxItem actionEventin, joka päivittää tietokentät esineen tiedoilla
		if(listViewPackageContent.getSelectionModel().isEmpty()) {
			return;
		}
		comboBoxItem.setValue((Items)listViewPackageContent.getSelectionModel().getSelectedItem());
		
		System.out.println("Näytetään esineen tiedot");
	}
	
	public void itemInfoChangeListeners() {
		//kuuntelijat, jotka säätävät esine nappulat riippuen siitä onko tietoja muutetttu vai ei.
		ChangeListener listener = (new ChangeListener() {
			@Override 
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				buttonSaveItem.setDisable(false);
				buttonRemoveItem.setDisable(false);
				if(!comboBoxItem.getSelectionModel().isEmpty()) {
					buttonUpdateItem.setDisable(false);
				}
				if(comboBoxItem.getValue() == newItem) {
					buttonUpdateItem.setDisable(true);
				}
			}
		});
		
		textFieldItemName.textProperty().addListener(listener);
		textFieldItemLength.textProperty().addListener(listener);
		textFieldItemWidth.textProperty().addListener(listener);
		textFieldItemHeight.textProperty().addListener(listener);
		textFieldItemWeight.textProperty().addListener(listener);
		textAreaItemDescription.textProperty().addListener(listener);
		radioButtonBreakable.selectedProperty().addListener(listener);
	}
	
	private UnaryOperator<Change> getFilter() {
		//filter, jota käytetään rajoittamaan painon ja mittojen syötettä sisältämään vain numeroita ja pisteitä
		return change -> {
			String text = change.getText();
			
			if(!change.isContentChange()) {
				return change;
			}
			if(text.matches("[0-9]*\\.?[0-9]*") || text.isEmpty()) {
				return change;
			}
			return null;
		};
	}
	
	private TextFormatter<String> getTextFormatter() {
		//käyttää yllä määriteltyä filteriä ja luo textFormatterin
		UnaryOperator<Change> filter = getFilter();
		TextFormatter<String> textFormatter = new TextFormatter<>(filter);
		return textFormatter;
	}
	
	private void itemTextFieldFormatters() {
		//textFormatter asetetaan eri tekstikenttiin
		textFieldItemLength.setTextFormatter(getTextFormatter());
		textFieldItemWidth.setTextFormatter(getTextFormatter());
		textFieldItemHeight.setTextFormatter(getTextFormatter());
		textFieldItemWeight.setTextFormatter(getTextFormatter());
	}
	
	
	
	//tab historia
	@FXML public void onButtonShowHistoryOnMap(ActionEvent event) {
		//piirtää kartalle reitin ja sen päätepisteet. Poistaa vanhat reitit. Tämä oli ennen oma nappinsa, nykyään tätä metodia vain kutsutaan muista metodeista
		onButtonRemovePaths(event);
		clearMarkers();
		
		String color = "purple";
		//otetaan valitun toimituksen tiedot
		Delivery d = (Delivery)listViewHistory.getSelectionModel().getSelectedItem();
		Pakettimaatti ps = d.getPs();
		Pakettimaatti pe = d.getPe();
		ArrayList<Double> coordinates = new ArrayList<Double>();
		coordinates.add(ps.getLat());
		coordinates.add(ps.getLng());
		coordinates.add(pe.getLat());
		coordinates.add(pe.getLng());
		
		addPoint(ps, color);
		addPoint(pe, color);
		
		//piirretään toimitus kartalle
		webView.getEngine().executeScript("document.createPath(" + coordinates + ", '" + color + "', '" + 0 + "')");
		System.out.println(coordinates.toString());
	}
	
	@FXML public void onListViewHistory(MouseEvent event) {
		//klikattaessa listViewia näyttää valitun toimituksen tarkemmat tiedot
		if(listViewHistory.getSelectionModel().isEmpty()) {
			return;
		}
		//kutsuu metodia joka piirtää reitin kartalle
		ActionEvent acEvent = new ActionEvent();
		onButtonShowHistoryOnMap(acEvent);
		//täydennetään eri listViewit toimituksen tiedoilla
		Delivery d = (Delivery)listViewHistory.getSelectionModel().getSelectedItem();
		listViewHistoryStart.getItems().clear();
		listViewHistoryStart.getItems().add(d.getPs().getKaupunki());
		listViewHistoryStart.getItems().add(d.getPs().getOsoite());
		listViewHistoryStart.getItems().add(d.getPs().getRakennus() + ", " + d.getPs().getTyyppi());
		for(Entry<String, String> e : d.getPs().getAika().entrySet()) {
			listViewHistoryStart.getItems().add(e.getKey() + ": " + e.getValue() + "\n");
		}
		listViewHistoryStart.getItems().add(d.getPs().getLat() + " " + d.getPs().getLng());
		
		listViewHistoryEnd.getItems().clear();
		listViewHistoryEnd.getItems().add(d.getPe().getKaupunki());
		listViewHistoryEnd.getItems().add(d.getPe().getOsoite());
		listViewHistoryEnd.getItems().add(d.getPe().getRakennus() + ", " + d.getPe().getTyyppi());
		for(Entry<String, String> e : d.getPe().getAika().entrySet()) {
			 listViewHistoryEnd.getItems().add(e.getKey() + ": " + e.getValue() + "\n");
		}
		listViewHistoryEnd.getItems().add(d.getPe().getLat() + " " + d.getPe().getLng());
		
		listViewHistoryItem.getItems().clear();
		String itemString = "";
		//esineiden tietojen lisääminen listViewiin. Lisäksi katsotaan ovatko esineet hajonneet
		boolean broken = false;
		for(Items item : d.getIl()) {
			broken = (item.getHajoava() == 1) && (d.getSc().getHajottava() == 1);
			itemString = item.getNimi() + " " + item.getPaino() + "kg " + String.format("%.5f", item.getKorkeus() * item.getLeveys() * item.getPituus()) + "m\u00B3 hajonnut: " + (broken? "kyllä":"ei");
			listViewHistoryItem.getItems().add(itemString);
		}
		
	}

	@FXML public void onButtonEditHistory(ActionEvent event) {
		//näyttää toimituksen tiedot paketinlähetys tabissa, niin että toimitusta voi muokata ja lähettää uudestaan. Ei muuta tietokannassa olevaa toimitusta
		//virheentarkistus
		if(listViewHistory.getSelectionModel().isEmpty()) {
			System.out.println("ListViewHistory: Ei valintaa");
			return;
		}
		
		System.out.println("Asetetaan arvot ohjelmaan");
		Delivery d = (Delivery)listViewHistory.getSelectionModel().getSelectedItem();
		
		//otetaan onActionit hetkeksi pois käytöstä, jotta kartalle ei piirretä turhaan moneen kertaan merkkejä, javascript/googlen api ei tykkää siitä
		//vaihdetaan comboBoxien arvot
		EventHandler<ActionEvent> handlerStart = comboBoxStart.getOnAction();
		EventHandler<ActionEvent> handlerEnd = comboBoxEnd.getOnAction();
		
		
		comboBoxStart.setOnAction(null);
		comboBoxEnd.setOnAction(null);
		comboBoxStart.setValue(d.getPs());
		comboBoxEnd.setValue(d.getPe());
		addMarkers(event);
		comboBoxStart.setOnAction(handlerStart);
		comboBoxEnd.setOnAction(handlerEnd);
		
		//valitaan paketti ja sen esineet
		for(Package p : listMan.getPl()) {
			if(p.getId() == d.getPakettiid()) {
				pack = p;
				comboBoxPackage.getSelectionModel().select(pack);
				listViewPackageContent.getItems().clear();
				break;
			}
		}
		
		//lisätään toimituksen sisältämät esineet toimituksesta, eikä pakettilistasta, sillä käyttäjä voi muokata pakettien tietoja
		listViewPackageContent.getItems().addAll(d.getIl());
		if(pack.getItems().size() != 0) {
			comboBoxItem.setValue(pack.getItems().get(0));
		}
		updatePackageTextFields();
		//valitaan lähetyksen luokka ja siirretään käyttäjä paketin lähetys tabiin
		comboBoxClass.setValue(d.getSc());
		tabPane.getSelectionModel().selectFirst();
	}
	
	@FXML public void onButtonRemoveFromHistory(ActionEvent event) {
		//poistaa reitin, paketin ja toimituksen listoista, comboBoxeista, listVieweistä ja tietokannasta
		//virheentarkistus
		if(listViewHistory.getSelectionModel().isEmpty()) {
			System.out.println("ListViewHistory: Ei valintaa");
			return;
		}
		//tietokannasta poistaminen ja listViewien tyhjentäminen
		Delivery d = (Delivery)listViewHistory.getSelectionModel().getSelectedItem();
		sqlMan.deleteDeliveryFromDatabase(d);
		listViewHistory.getItems().remove(d);
		listViewHistoryStart.getItems().clear();
		listViewHistoryEnd.getItems().clear();
		listViewHistoryItem.getItems().clear();
		
		//paketin poistaminen
		Package pack = null;
		for(Package p : listMan.getPl()) {
			if(p.getId() == d.getPakettiid()) {
				pack = p;
				break;
			}
		}
		listMan.getPl().remove(pack);
		listMan.getDl().remove(d);
		
		//kartan tyhjentäminen
		clearMarkers();
		onButtonRemovePaths(event);
		System.out.println("Toimitus poistettu tietokannasta");
	}
	
	@FXML public void onDatePickerHistory(ActionEvent event) {
		if(datePickerHistory.getTypeSelector().isEmpty()) {
			return;
		}
		if(datePickerHistory.getEditor().getText().isEmpty()) {
			listViewHistory.getItems().clear();
			listViewHistory.getItems().addAll(listMan.getDl());
			anchorPane.requestFocus();
			return;
		}
		DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String date = df.format(datePickerHistory.getValue());
		System.out.println(date);
		
		listViewHistory.getItems().clear();
		for(Delivery d : listMan.getDl()) {
			if(d.getTime().split(" ")[0].equals(date)) {
				listViewHistory.getItems().add(d);
			}
		}
	}
	
	//tab asetukset
	@FXML public void onButtonDefaultDatabase(ActionEvent event) {
		//nollaa databasen, listat ja comboboxit + listViewit. Katkaisee yhteyden muistidatabaseen ja poistaa database tiedoston. Initialisoi ohjelman uudestaan
		log.writeToLog("*** Aloitetaan tietokannan nollaaminen ***");
		try {
			sqlMan.getConnection().close();
			FileManager fm = new FileManager();
			fm.deleteFile("db.db");
		} catch(SQLException sqle) {
			sqle.getLocalizedMessage();
		}
		listMan.getAl().clear();
		listMan.getIl().clear();
		listMan.getPl().clear();
		listMan.getScl().clear();
		listMan.getDl().clear();
		
		//nollataan asiat
		textFieldStart.setText("");
		textFieldEnd.setText("");
		textFieldItemName.setText("");
		textFieldItemHeight.setText("");
		textFieldItemWidth.setText("");
		textFieldItemLength.setText("");
		textFieldItemVolume.setText("");
		textFieldItemWeight.setText("");
		textAreaItemDescription.setText("");
		textFieldPackageSize.setText("");
		textFieldPackageWeight.setText("");
		radioButtonBreakable.setSelected(false);
		
		pack.clear();
		listViewPackageContent.getItems().clear();
		listViewHistoryStart.getItems().clear();
		listViewHistoryEnd.getItems().clear();
		listViewHistoryItem.getItems().clear();
		
		//kutsutaan initialize metodia, joka luo tietokannan uudestaan
		initialize(null, null);
		listViewClassInfo.getItems().clear();
		log.writeToLog("*** Tietokanta nollattu ***");
	}
	
	

	
	@Override
    public void initialize(URL url, ResourceBundle rb) {
		//alustaa käyttöliittymän, tietokannan ja ohjelman
		long startTime = System.currentTimeMillis();
		log.writeToLog("Ohjelma käynnistetty ");
		webView.getEngine().load(getClass().getResource("index.html").toExternalForm());
		labelShowDist.setText("");
		labelSendInfo.setText("");
		
		/*Karkaamisen esto*/ 
		//estää ettei kartasta pääse pakenemaan internettiin linkkien kautta
		//metodi lisää kuuntelijoita, jotka tunnistavat jos linkin lataaminen alkaa, ja estää sen lataamisen
		webView.setContextMenuEnabled(false);
		webView.getEngine().getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
			@Override
			public void changed(ObservableValue observable, State oldState, State newState) {
				if(newState == Worker.State.SUCCEEDED) {
					EventListener listener = new EventListener() {
						@Override
						public void handleEvent(org.w3c.dom.events.Event event) {
							event.preventDefault();
						}
					};
				
					Document doc = webView.getEngine().getDocument();
					NodeList nodes = doc.getChildNodes();
					for(int i=0;i<nodes.getLength();i++) {
						((EventTarget) nodes.item(i)).addEventListener("click", listener, false);
					}
				}
			}
		});

		
		//tietokannan alustaminen
		SQLManager sqlMan = SQLManager.getInstance();
		if(!sqlMan.createDatabase()) {
			sqlMan.fillDatabaseFromFile("database.sql");
			sqlMan.fillDatabaseFromFile("putstuffindatabase.sql");
			XMLParser XMLPar = new XMLParser();
			XMLPar.god();
		}
		//pakettiautomaattien hakeminen
		sqlMan.getAllAutomaton();
		comboBoxStart.getItems().clear();
		comboBoxEnd.getItems().clear();
		comboBoxStart.getItems().addAll(listMan.getAl());
		comboBoxEnd.getItems().addAll(listMan.getAl());
		
		//esineiden hakeminen
		sqlMan.getItems();
		comboBoxItem.getItems().clear();
		comboBoxItem.getItems().add(newItem);
		comboBoxItem.getItems().addAll(listMan.getIl());
		
		//lähetysluokkien hakeminen
		sqlMan.getSendClass();
		comboBoxClass.getItems().clear();
		comboBoxClass.getItems().addAll(listMan.getScl());
		itemInfoChangeListeners();
		itemTextFieldFormatters();
		
		//toimitusten hakeminen
		sqlMan.getAllDeliveryFromDatabase();
		listViewHistory.getItems().clear();
		listViewHistory.getItems().addAll(listMan.getDl());
		
		//pakettien hakeminen
		sqlMan.getAllPackageFromDatabase();
		comboBoxPackage.getItems().clear();
		comboBoxPackage.getItems().addAll(listMan.getPl());
		
		long endTime = System.currentTimeMillis();
		System.out.println("Käynnistysaika: " + (endTime - startTime) + "ms");
		log.writeToLog("Ohjelman käynnistysaika: " + (endTime - startTime + "ms"));
		
    }   
	
}
