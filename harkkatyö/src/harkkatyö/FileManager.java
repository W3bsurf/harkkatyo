package harkkatyö;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileManager {

	public String readFile(String url) {
		//lukee tiedoston annetusta polusta ja palauttaa sen koko sisällön stringinä
		String line = "";
		String content = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(url));
			while((line = br.readLine()) != null) {
				content += line;
			}
			br.close();
		} catch(IOException e) {
			System.out.println("virhe luettaessa tai avattaessa tiedostoa");
		}
		return content;
	}
	
	public void deleteFile(String url) {
		//poistaa tiedoston annetusta polusta
		File file = new File(url);
		boolean b = file.delete();
		System.out.println(b? "Tiedoston poistaminen ei onnistunut: tiedostoa ei ole" : "Tiedosto poistettu onnistuneesti");
	}
}
