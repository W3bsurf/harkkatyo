package harkkatyö;

import java.util.ArrayList;

public class Package {
	//javassa nimellä paketti, mutta vastaa tietokannan tablea "kasa"
	private int id;
	private ArrayList<Items> al = new ArrayList<Items>();
	
	public Package() {
		al = new ArrayList<Items>();
		id = 0;
	}
	
	public Package(int i, ArrayList<Items> list) {
		al.addAll(list);
		id = i;
	}
	
	@Override public String toString() {
		//override toString metodi, jotta objecteja voisi lisätä comboBoxeihin ja listVieveihin helposti
		String content = "Paketti: ";
		for(Items item : al) {
			content += item.getNimi() + " ";
		}
		return id + ". " + content;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int i) {
		this.id = i;
	}
	
	public void addItem(Items item) {
		al.add(item);
	}
	
	public void addItems(ArrayList<Items> list) {
		al.addAll(list);
	}
	
	public void removeItem(Object o) {
		al.remove(o);
	}
	
	public void removeAll(Object o) {
		while(al.remove(o));
	}
	
	public void clear() {
		al.clear();
	}
	
	public void removeItem(int index) {
		al.remove(index);
	}
	
	public double getCompleteWeight() {
		double d = 0;
		for(Items item : al) {
			d += item.getPaino();
		}
		return d;
	}
	
	//pakkausmetodi, joka sen sijaan että laskee esineiden tilavuudet yhteen, pyrkii laskemaan realistisemman koon esineiden vaatimalle tilalle
	//perustuu siihen, että kahden esineen lyhyimmät sivut laitetaan niin yhteen, toisin sanottuna esineiden isoimmat pinnat laitetaan vastakkain
	//ja ne muodostavat uuden esineen, johon sitten lisätään seuraava esine, kunnes kaikki esineet on lisätty pakettiin
	//toimii huonosti kuutioilla(vie mahdollisimman paljon tilaa, eikä mahdollisimman vähän)
	public double getCompleteSize() {
		double d = 0;
		ArrayList<Items> order = new ArrayList<Items>();
		Items largestItem = null;
		
		/*listan järjestäminen suurimmasta pienimpään*/
		while(al.size() > 0) {
			d = 0;
			for(Items item : al) {
				if(item.getPituus() * item.getKorkeus() * item.getLeveys() > d) {
					d = item.getPituus() * item.getKorkeus() * item.getLeveys();
					largestItem = item;
				}
			}
			order.add(largestItem);
			al.remove(largestItem);
		}
		al = order;
		d = 0;
		//alustetaan paketin koko
		double[] packSize = {0, 0, 0};
		
		
		for(Items item : al) {
			//laitetaan esineen mitat listaan
			double[] itemSize = {item.getKorkeus(), item.getLeveys(), item.getPituus()};
			
			//jos paketin koko on nolla, niin ensimmäinen esine määrittää paketin sivut
			if(packSize[0] * packSize[1] * packSize[2] == 0) {
				packSize[0] = item.getKorkeus();
				packSize[1] = item.getLeveys();
				packSize[2] = item.getPituus();
			} else {
				/*selvitetään paketin lyhyin sivu*/
				double shortSide = 1000000;
				int index = 0;
				for(int i=0;i<3;i++) {
					if(shortSide > packSize[i]) {
						shortSide = packSize[i];
						index = i;
					}
				}
				/*selvitetään esineen lyhyin sivu*/
				double shortSideItem = 1000000;
				int indexItem = 0;
				for(int i=0;i<3;i++) {
					if(shortSideItem > itemSize[i]) {
						shortSideItem = itemSize[i];
						indexItem = i;
					}
				}
				
				//lyhyimmät sivut voidaan laittaa päällekkäin kahdella eri tapaa(samansuuntaisesti/ristikkäin), joten kokeillaan molemmat vaihtoehdot ja valitaan niistä
				//tilavuudeltaan pienin ratkaisu
				double height = 0;
				double width = 0;
				double length = 0;
				double width2 = 0;
				double length2 = 0;
				double firstOri = 0;
				double secondOri = 0;
				height = packSize[index] + itemSize[indexItem];
				
				width = Math.max(packSize[getLargerIndex(index)], itemSize[getLargerIndex(indexItem)]);
				length = Math.max(packSize[getSmallerIndex(index)], itemSize[getSmallerIndex(indexItem)]);
				firstOri = height * width * length;
				
				width2 = Math.max(packSize[getLargerIndex(index)], itemSize[getSmallerIndex(indexItem)]);
				length2 = Math.max(packSize[getSmallerIndex(index)], itemSize[getLargerIndex(indexItem)]);
				secondOri = height * width2 * length2;
				
				 if(firstOri <= secondOri) {
					 packSize[0] = height;
					 packSize[1] = width;
					 packSize[2] = length;
				 } else {
					 packSize[0] = height;
					 packSize[1] = width2;
					 packSize[2] = length2;
				 }
					
//				System.out.println("\nEsine:\t\t" + itemSize[0] + "x" + itemSize[1] + "x" + itemSize[2] + "\npaketti:\t" + packSize[0] + "x" + packSize[1] + "x" + packSize[2]);
			}
		}
		return packSize[0] * packSize[1] * packSize[2];
	}
	
	public int getLargerIndex(int i) {
		//käytetään valitsemaan seuraava sivu. Liittyy tilavuudenlaskijaan
		if(i == 0) return 1;
		if(i == 1) return 2;
		if(i == 2) return 0;
		return 0;
	}
	
	public int getSmallerIndex(int i) {
		//käytetään valitsemaan edellinen sivu. Liittyy tilavuudenlaskijaan
		if(i == 0) return 2;
		if(i == 1) return 0;
		if(i == 2) return 1;
		return 0;
	}
	
	public ArrayList<Items> getItems() {
		return al;
	}
	
	public void setItem(Object o) {
		if(al.contains(o)) {
			al.set(al.indexOf(o), (Items)o);
		}
	}
}
