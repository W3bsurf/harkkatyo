package harkkatyö;

public class SendClass {
	//lähetysluokka
	private int luokka;
	private double kokoraja;
	private double painoraja;
	private double etäisyysraja;
	private double nopeus;
	private int hajottava;
	
	public SendClass(int l, double kr, double pr, double er, double n, int h) {
		luokka = l;
		kokoraja = kr;
		painoraja = pr;
		etäisyysraja = er;
		nopeus = n;
		hajottava = h;
	}
	
	@Override public String toString() {
		//override toString metodi, jotta objecteja voisi lisätä comboBoxeihin ja listVieveihin helposti
		return Integer.toString(luokka) + ". luokka";
	}

	public int getLuokka() {
		return luokka;
	}

	public double getKokoraja() {
		return kokoraja;
	}

	public double getPainoraja() {
		return painoraja;
	}

	public double getEtäisyysraja() {
		return etäisyysraja;
	}

	public double getNopeus() {
		return nopeus;
	}

	public int getHajottava() {
		return hajottava;
	}
	

	public void setLuokka(int luokka) {
		this.luokka = luokka;
	}

	public void setKokoraja(double kokoraja) {
		this.kokoraja = kokoraja;
	}

	public void setPainoraja(double painoraja) {
		this.painoraja = painoraja;
	}

	public void setEtäisyysraja(double etäisyysraja) {
		this.etäisyysraja = etäisyysraja;
	}

	public void setNopeus(double nopeus) {
		this.nopeus = nopeus;
	}

	public void setHajottava(int hajottava) {
		this.hajottava = hajottava;
	}
	
}
