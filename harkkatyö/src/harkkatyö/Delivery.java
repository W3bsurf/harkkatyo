package harkkatyö;

import java.util.ArrayList;

public class Delivery {
	//luokka toimituksille
	private Pakettimaatti ps;
	private Pakettimaatti pe;
	private SendClass sc;
	private ArrayList<Items> il;
	private String time;
	private int pakettiid;
	private int reittiid;
	
	public Delivery(Pakettimaatti pstart, Pakettimaatti pend, SendClass sendClass, ArrayList<Items> list, String t, int pid, int rid) {
		ps = pstart;
		pe = pend;
		sc = sendClass;
		il = list;
		time = t;
		pakettiid = pid;
		reittiid = rid;
	}
	
	@Override
	//override toString metodi, jotta objecteja voisi lisätä comboBoxeihin ja listVieveihin helposti
	public String toString() {
		String itemString = "";
			for(Items item : il) {
				itemString += item.toString() + " ";
				
			}
		return time + " | " + ps.getKaupunki()+ "->" + pe.getKaupunki() + " luokka: " + sc.getLuokka() + ". esineet: " + itemString;
		
	}
	
	public Pakettimaatti getPs() {
		return ps;
	}

	public Pakettimaatti getPe() {
		return pe;
	}

	public SendClass getSc() {
		return sc;
	}

	public ArrayList<Items> getIl() {
		return il;
	}
	
	public String getTime() {
		return time;
	}
	
	public int getPakettiid() {
		return pakettiid;
	}
	
	public int getReittiid() {
		return reittiid;
	}

	public void setPs(Pakettimaatti ps) {
		this.ps = ps;
	}

	public void setPe(Pakettimaatti pe) {
		this.pe = pe;
	}

	public void setSc(SendClass sc) {
		this.sc = sc;
	}

	public void setIl(ArrayList<Items> il) {
		this.il = il;
	}

}
