package harkkatyö;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LogWriter {
	private String fileName = "log.log";
	
	
	public void writeToLog(String text) {
		//avaa/luo tiedoston ja lisää sinne annetun stringin tekstiä. Laittaa myös aikaleiman tekstiin
		try {
			File file = new File(fileName);
			if(!file.exists()) {
				file.createNewFile();
			}
			SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
			String date = ft.format(new Date());
			FileWriter fw = new FileWriter(file, true);
			fw.append(date + ": " + text + "\n");
			fw.close();
		} catch(IOException ioe) {
			System.out.println("Lokitiedoston avaamisessa onglemia");
		}
	}

}
