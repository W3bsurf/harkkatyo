package harkkatyö;

import java.util.ArrayList;

public class ListManager {
	//singleton luokka, jonka kautta päästään käsiksi kaikkiin listoihin
	//sisältää myös listakohtaisia erikoismetodeja, joita tarvitaan tietyissä tilanteissa
	private ArrayList<Pakettimaatti> al = new ArrayList<Pakettimaatti>();
	private ArrayList<Items> il = new ArrayList<Items>();
	private ArrayList<SendClass> scl = new ArrayList<SendClass>();
	private ArrayList<Delivery> dl = new ArrayList<Delivery>();
	private ArrayList<Package> pl = new ArrayList<Package>();
	
	static private ListManager lm = null;
	
	public ArrayList<Pakettimaatti> getAl() {
		return al;
	}


	public ArrayList<Items> getIl() {
		return il;
	}


	public ArrayList<SendClass> getScl() {
		return scl;
	}


	public ArrayList<Delivery> getDl() {
		return dl;
	}


	public ArrayList<Package> getPl() {
		return pl;
	}


	public static ListManager getLm() {
		return lm;
	}


	public void setAl(ArrayList<Pakettimaatti> al) {
		this.al = al;
	}


	public void setIl(ArrayList<Items> il) {
		this.il = il;
	}


	public void setScl(ArrayList<SendClass> scl) {
		this.scl = scl;
	}


	public void setDl(ArrayList<Delivery> dl) {
		this.dl = dl;
	}


	public void setPl(ArrayList<Package> pl) {
		this.pl = pl;
	}


	public static void setLm(ListManager lm) {
		ListManager.lm = lm;
	}
	
	
	
	
	static public ListManager getInstance() {
		if(lm == null) {
			lm = new ListManager();
		}
		return lm;
	}
	
	
	public int itemIndexOf(Items item) {
		for(int i=0;i<il.size();i++) {
			if(item.getId() == il.get(i).getId()) {
				return i;
			}
		}
		return -1;
	}
	
	public void printListContents() {
		for(Pakettimaatti p : al) {
			System.out.println(p.getKaupunki() + "\n" + p.getOsoite() + "\n");
		}
	}
	
	public Pakettimaatti getAutomaton(double lat, double lng) {
		for(Pakettimaatti p : al) {
			if(p.getLat() == lat && p.getLng() == lng) {
				return p;
			}
		}
		return null;
	}
	
	public SendClass getSendClass(int i) {
		for(SendClass s : scl) {
			if(s.getLuokka() == i) {
				return s;
			}
		}
		return null;
	}
	
	public Items getItem(int id) {
		for(Items i : il) {
			if(i.getId() == id) {
				return i;
			}
		}
		return null;
	}
}
