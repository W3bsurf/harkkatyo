package harkkatyö;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLParser {	
	
	public void god() {
		//suorittaa xml:än parsaamisen oikeassa järjestyksessä
		String s = "http://smartpost.ee/fi_apt.xml";
		String xml = getXML(s);
		System.out.println("xml haettu");
		build(xml);
	}
	
	public String getXML(String s) {
		//lukee xml tiedoston annetusta osoitteesta
		String content = "";
		String line = "";
		try {
			URL url = new URL(s);
			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
			while((line = br.readLine()) != null) {
				content += line;
			}
		} catch (MalformedURLException murle) {
			murle.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return content;
	}
	
	public void build(String s) {
		//rakennetaan document annetusta stringistä
		Document doc;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			
			doc = db.parse(new InputSource(new StringReader(s)));
			doc.getDocumentElement().normalize();
			
			System.out.println("Aloitetaaan parsaaminen ja automaattien lisääminen tietokantaan");
			parse(doc);
			
			
		} catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		} catch (SAXException saxe) {
			saxe.printStackTrace();
		}
	}
	
	public void parse(Document doc) {
		//parsitaan tiedot documentista haluttuun muotoon ja lisätään ne tietokantaan ja arrayListaan
		SQLManager sqlMan = SQLManager.getInstance();
		NodeList nodes = doc.getElementsByTagName("place");
		for(int i=0; i<nodes.getLength(); i++) {
			Node node = nodes.item(i);
			Element e = (Element) node;
			double lat = Double.parseDouble(fetch("lat", e, 0));
			double lng = Double.parseDouble(fetch("lng", e, 0));
			
			String osoite = fetch("address", e, 0);
			String rakennus = fetch("postoffice", e, 0).split(",|\\s+", 2)[1].trim();
			String tyyppi = fetch("postoffice", e, 0).split(",|\\s+", 2)[0];
			String nimi = fetch("city", e, 0);
			String postinumero = fetch("code", e, 0);
			LinkedHashMap<String, String> aika = new LinkedHashMap<String, String>();
			aika = timeMapper(fetch("availability", e, 0));
			Pakettimaatti p = new Pakettimaatti(lat, lng, osoite, rakennus, tyyppi, nimi, postinumero, aika);
			//al.add(p);
			sqlMan.addBoxAutomatonToDatabase(p);
		}
		sqlMan.printStats();
	}
	
	public String fetch(String tag, Element e, int i) {
		//noudetaan halutut tiedot elementistä
		//System.out.println(e.getElementsByTagName(tag).item(i).getTextContent());
		return(e.getElementsByTagName(tag).item(i).getTextContent());
	}
	
	public LinkedHashMap<String, String> timeMapper(String s) {
		//Ajan parsiminen muotoon, jossa joka päivälle on erikseen aloitusaika ja sulkemisaika
		//huonosta xml:stä, jossa ajat on ilmoitettu usealla eri tavalla, johtuen parsii onnistuneesti vain noin 95% automaateista
		LinkedHashMap<String, String> h = new LinkedHashMap<String, String>();
		ArrayList<String> day = ListOfDays.getListOfDays();
		if(s.equals("24h")) {
			for(String d : day) {
				h.put(d, "24h");
			}
		} else if(s.length() > 0){
			int alku = 0;
			int loppu = 0;
			String aika = "";
			for (String d : s.split(",")) {
				for(String d2 : d.split("\\s+")) {
					if(d2.equals("-")) {
						//System.out.println("viiva");
						continue;
					}
					
					if(d2.matches("\\D{2}")) {
						alku = dayFinder(d2, day);
						loppu = alku;
						//System.out.println("yksittäinen päivä");
					}
					else if(d2.matches("\\D{2}" + "-" + "\\D{2}")) {
						alku = dayFinder(d2.split("-")[0], day);
						loppu = dayFinder(d2.split("-")[1], day);
						//System.out.println("päiväväli");
					}
					else if(d2.matches("\\d" + "." + "\\d{2}")) {
						if(aika.equals("")) aika = d2;
						else aika += "-" + d2;
						//System.out.println("lyhyt kellonaika");
					}
					
					else if(d2.matches("\\d{2}" + "." + "\\d{2}")) {
						if(aika.equals("")) aika = d2;
						else aika += "-" + d2;
						//System.out.println("pitkä kellonaika");
					}
				}
				//System.out.println("lisätään päivät");
				for(int i=alku;i<loppu+1;i++) {
					//System.out.println(day.get(i) + " " + aika);
					h.put(day.get(i), aika);
				}
				aika = "";
			}
		}
		//lisätään päivät jolloin automaatti kiinni
		for(String da : day) {
			if(h.containsKey(da) == false) {
				h.put(da, "kiinni");
			}
		}
		//System.out.println(h.size());
		return h;
		
	}
	/*  ma-pe 7.00 - 21.00, la 7.00 - 18.00, su 12.00 - 18.00  */
	
	
	public int dayFinder(String s, ArrayList<String> day) {
		//etsii päivän päivälistasta
		int i=0;
		for(i=0;i<day.size();i++) {
			if(s.equals(day.get(i))) {
				break;
			}
		}
		return i;
	}
	
	

}
